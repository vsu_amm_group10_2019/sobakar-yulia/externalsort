﻿
namespace WindowsFormsApp1
{
    partial class FormCheck
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxRandom = new System.Windows.Forms.TextBox();
            this.textBoxBack = new System.Windows.Forms.TextBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.buttonCreate = new System.Windows.Forms.Button();
            this.buttonSort = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxRandom
            // 
            this.textBoxRandom.Location = new System.Drawing.Point(27, 170);
            this.textBoxRandom.Multiline = true;
            this.textBoxRandom.Name = "textBoxRandom";
            this.textBoxRandom.ReadOnly = true;
            this.textBoxRandom.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxRandom.Size = new System.Drawing.Size(351, 340);
            this.textBoxRandom.TabIndex = 0;
            // 
            // textBoxBack
            // 
            this.textBoxBack.Location = new System.Drawing.Point(492, 170);
            this.textBoxBack.Multiline = true;
            this.textBoxBack.Name = "textBoxBack";
            this.textBoxBack.ReadOnly = true;
            this.textBoxBack.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxBack.Size = new System.Drawing.Size(351, 340);
            this.textBoxBack.TabIndex = 1;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(27, 28);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 22);
            this.numericUpDown1.TabIndex = 2;
            // 
            // buttonCreate
            // 
            this.buttonCreate.Location = new System.Drawing.Point(188, 12);
            this.buttonCreate.Name = "buttonCreate";
            this.buttonCreate.Size = new System.Drawing.Size(111, 53);
            this.buttonCreate.TabIndex = 3;
            this.buttonCreate.Text = "Создать";
            this.buttonCreate.UseVisualStyleBackColor = true;
            this.buttonCreate.Click += new System.EventHandler(this.buttonCreate_Click);
            // 
            // buttonSort
            // 
            this.buttonSort.Enabled = false;
            this.buttonSort.Location = new System.Drawing.Point(339, 13);
            this.buttonSort.Name = "buttonSort";
            this.buttonSort.Size = new System.Drawing.Size(111, 52);
            this.buttonSort.TabIndex = 4;
            this.buttonSort.Text = "Сортировать";
            this.buttonSort.UseVisualStyleBackColor = true;
            this.buttonSort.Click += new System.EventHandler(this.buttonSort_Click);
            // 
            // FormCheck
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(886, 522);
            this.Controls.Add(this.buttonSort);
            this.Controls.Add(this.buttonCreate);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.textBoxBack);
            this.Controls.Add(this.textBoxRandom);
            this.Name = "FormCheck";
            this.Text = "FormCheck";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxRandom;
        private System.Windows.Forms.TextBox textBoxBack;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button buttonCreate;
        private System.Windows.Forms.Button buttonSort;
    }
}