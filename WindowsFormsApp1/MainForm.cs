﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            Draw();
        }


        private void Draw()
        {
            dataGridView.Rows.Add();
            dataGridView.Rows[0].Cells[1].Value = "Случайный файл";
            dataGridView.Rows[0].Cells[2].Value = "Обратный файл";
            dataGridView.Rows[0].Cells[3].Value = "Случайный файл";
            dataGridView.Rows[0].Cells[4].Value = "Обратный файл";
            dataGridView.Rows[0].Cells[5].Value = "Случайный файл";
            dataGridView.Rows[0].Cells[6].Value = "Обратный файл";
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            int countElem = Convert.ToInt32(numericUpDown.Value);
            int countRow = dataGridView.Rows.Count;
            dataGridView.Rows.Add();
            dataGridView.Rows[countRow].Cells[0].Value = countElem;
            
            Utils.GenerateRandomFile("Random", countElem);
            Utils.GenerateReverseFile("Back", countElem);

            TwoPhaseSimpleSort sort = new TwoPhaseSimpleSort();
            Stopwatch stopWatch = new Stopwatch();

            stopWatch.Start();
            sort.SortFile("Random");
            stopWatch.Stop();
            TimeSpan timeRandom = stopWatch.Elapsed;
            dataGridView.Rows[countRow].Cells[1].Value = sort.countPasses;
            dataGridView.Rows[countRow].Cells[3].Value = sort.countComparisons;
            dataGridView.Rows[countRow].Cells[5].Value = stopWatch.Elapsed; 

            stopWatch.Restart();
            sort.SortFile("Back");
            stopWatch.Stop();
            dataGridView.Rows[countRow].Cells[2].Value = sort.countPasses;
            dataGridView.Rows[countRow].Cells[4].Value = sort.countComparisons;
            dataGridView.Rows[countRow].Cells[6].Value = stopWatch.Elapsed;

        }

        private void buttonCheck_Click(object sender, EventArgs e)
        {
            FormCheck formCheck = new FormCheck();
            formCheck.ShowDialog();
        }
    }
}
