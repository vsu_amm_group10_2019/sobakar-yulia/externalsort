﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class FormCheck : Form
    {
        public FormCheck()
        {
            InitializeComponent();
        }

        private void buttonCreate_Click(object sender, EventArgs e)
        {
            int countElem = Convert.ToInt32(numericUpDown1.Value);
            Utils.GenerateRandomFile("Random", countElem);
            Utils.GenerateReverseFile("Back", countElem);
            textBoxRandom.Text = Utils.PrintFile("Random");
            textBoxBack.Text = Utils.PrintFile("Back");

            buttonCreate.Enabled = false;
            buttonSort.Enabled = true;
        }

        private void buttonSort_Click(object sender, EventArgs e)
        {
            buttonCreate.Enabled = true;
            buttonSort.Enabled = false;

            TwoPhaseSimpleSort sort = new TwoPhaseSimpleSort();

            sort.SortFile("Random");
            textBoxRandom.Text = Utils.PrintFile("Random");

            sort.SortFile("Back");
            textBoxBack.Text = Utils.PrintFile("Back");

        }
    }
}
