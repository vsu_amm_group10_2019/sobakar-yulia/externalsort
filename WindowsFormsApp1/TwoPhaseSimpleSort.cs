﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class TwoPhaseSimpleSort //Двухпутевая двухфазная простым слиянием
    {
        const int filesCount = 2;
        public int countComparisons { get; private set; }
        public int countPasses { get; private set; }

        public void Merge(Sequence sequence, int length, params Sequence[] sequences)
        {
            sequence.StartWrite();
            foreach (Sequence tmp in sequences)
            {
                tmp.StartRead();
                tmp.NewRun(length);
                tmp.ReadElem();
            }
            while (FirstReady(sequences) > -1)
            {
                while (FirstReady(sequences) > -1)
                {
                    int i = GetIndexOfMin(sequences);
                    sequences[i].CopyElementTo(sequence);
                    sequences[i].ReadElem();
                }
                foreach (Sequence tmp in sequences)
                {
                    tmp.NewRun(length);
                    tmp.ReadElem();
                }
            }
            foreach (Sequence tmp in sequences)
            {
                tmp.StopRead();
            }
            sequence.StopWrite();
        }
        public void Distribute(Sequence sequence, int length, params Sequence[] sequences)
        {
            sequence.StartRead();
            sequence.NewRun(length);
            foreach (Sequence tmp in sequences)
            {
                tmp.StartWrite();
            }
            int i;
            while (!sequence.IsEndOfFile())
            {
                i = 0;
                while (!sequence.IsEndOfFile() && i < sequences.Length)
                {
                    sequence.CopyAllTo(sequences[i]);
                    sequence.NewRun(length);
                    i++;
                }
            }
            foreach (Sequence tmp in sequences)
            {
                tmp.StopWrite();
            }
            sequence.StopRead();
        }
        private Sequence[] InitSequences(int count)
        {
            Sequence[] sequences = new Sequence[count];
            for (int i = 0; i < count; i++)
            {
                sequences[i] = new Sequence("tmp" + i.ToString());
            }
            return sequences;
        }
        private int FirstReady(Sequence[] sequences)
        {
            int first = 0;
            while (first < sequences.Length && sequences[first].ReadyToCopy == false)
            {
                first++;
            }
            if (first == sequences.Length)
            {
                return -1;
            }
            return first;
        }
        private int GetIndexOfMin(Sequence[] sequences)
        {
            int max = FirstReady(sequences);
            if (max == -1)
            {
                throw new Exception("Пусто");
            }
            for (int i = max + 1; i < sequences.Length; i++)
            {
                
                if (sequences[i].ReadyToCopy && sequences[i].Element < sequences[max].Element)
                {
                    max = i;   
                }
                countComparisons++;
            }
            return max;
        }
        public void SortFile(string fileName)
        {
            countComparisons = 0;
            countPasses = 0;
            FileInfo fileInfo = new FileInfo(fileName);
            long n = fileInfo.Length / sizeof(Int32);
            Sequence sequence = new Sequence(fileName);
            Sequence[] sequences = InitSequences(filesCount);
            int length = 1;
            do
            {
                Distribute(sequence, length, sequences);
                Merge(sequence, length, sequences);
                length *= filesCount;
                countPasses++;
            } while (length < n);
            foreach (Sequence tmp in sequences)
            {
                tmp.FileInfo.Delete();
                tmp.Dispose();
            }
            sequence.Dispose();
        }

    }

}
